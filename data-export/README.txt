License
----------------------------------------
This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

Organization
----------------------------------------
There are four experiments - labeled as P0-P3.  For each experiment there are two channels of fluorometer data - labeled as C0-C1.  Each experiment and channel is a scalar time series and is distributed as a CSV file.  An image file with the same name is also supplied as a redundant check.

CSV File Description.
----------------------------------------
The first column of the CSV file is the the UNIX time in seconds of the measurment.

The second column of the CSV file is the concentration value in parts-per-billion.  This concentration is estimated from the analog sensor output by applying calibration routines determined in the laboratory.
