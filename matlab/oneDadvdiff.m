
xx = linspace(0,10,100);
t = [1,10,100];
C0 = 1.0;
u = 0.1;
Dx = 0.001;

figure(1);
clf()
for ii = 1:length(t)
    c = C0/2*erfc((xx-u*t(ii))/sqrt(4*Dx*t(ii)));
    plot(xx,c)
    hold on
end

