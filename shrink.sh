#!/bin/bash
IN=plume-pub.pdf
OUT=plume-pub-compressed.pdf
gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.5 -dNOPAUSE -dQUIET \
    -dBATCH -dPDFSETTINGS=/default -r72 \
    -sOutputFile=$OUT $IN
